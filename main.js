let mlist = document.querySelector('.el_mlist');

mlist.addEventListener('click', function (ev) {
    // console.log(ev);
    // // console.log(ev.target.classList);
    if (ev.target.classList.contains('el_check')) {
        if (ev.target.parentElement.classList.contains('in_edit')) {
            return;
        }
        let el_span = ev.target.parentElement.querySelector('span');
        if (ev.target.checked) {
            el_span.classList.add('text-decoration-line-through');
        } else {
            el_span.classList.remove('text-decoration-line-through');
        }
        saveToStorage();
    } else if (ev.target.classList.contains('btn_delete')) {
        if (ev.target.parentElement.classList.contains('in_edit')) {
            return;
        }
        let el = ev.target.parentElement;
        el.parentElement.removeChild(el);
        saveToStorage();
    } else if (ev.target.classList.contains('task_span')) {
        let allInEdit = document.querySelectorAll(".in_edit");
        for (let i = 0; i < allInEdit.length; i++) {
            allInEdit[i].classList.remove('in_edit');
        }
        ev.target.parentElement.classList.add('in_edit');
        let old_input = document.querySelector('#new_task');
        old_input.value = ev.target.innerText;
        let addButtonEl = document.querySelector('#btn_new_task');
        addButtonEl.innerText = 'Обновить';
        addButtonEl.classList.add('js-edit-button');
        addButtonEl.classList.remove('js-add-button');
        let elXGroup = document.querySelector('#clear-edit-group');
        elXGroup.classList.remove('d-none');
        ev.target.parentElement.querySelector('.el_check').setAttribute('disabled', 'disabled');
    }

});

const addButtonEl = document.querySelector('#btn_new_task');

addButtonEl.addEventListener('click',
    function (ev) {
        if (ev.target.classList.contains('js-add-button')) {
            let newTaskInput = ev.target.parentElement.querySelector("#new_task");
            let newTask = newTaskInput.value;
            if (newTask == "") {
                return;
            }

            let mlist = document.querySelector('.el_mlist');

            let newListItem = document.createElement('li');
            newListItem.classList.add('list-group-item');

            let newInput = document.createElement('input');
            newInput.classList.add('form-check-input');
            newInput.classList.add('el_check', 'mr-1');
            newInput.setAttribute('type', 'checkbox');
            newListItem.appendChild(newInput);


            let newSpan = document.createElement('span');
            newSpan.classList.add('task_span');
            newSpan.innerText = newTask;
            newListItem.appendChild(newSpan);

            let newBtnDel = document.createElement('button');
            newBtnDel.classList.add('btn_delete', 'btn', 'btn-danger', 'btn-sm', 'rounded-0');
            newBtnDel.setAttribute('type', 'button');
            newBtnDel.setAttribute('title', 'delete');
            newBtnDel.setAttribute('data-toggle', 'tooltip');
            newBtnDel.setAttribute('data-placement', 'top');
            newBtnDel.innerText = 'x';
            newListItem.appendChild(newBtnDel);

            mlist.appendChild(newListItem);

            newTaskInput.value = "";
            saveToStorage();
        } else if (ev.target.classList.contains('js-edit-button')) {
            let old_input = document.querySelector('#new_task');
            if (old_input.value == "") {
                alert('Введите текст');
                return;
            }
            ev.target.innerText = 'Добавить';
            ev.target.classList.add('js-add-button');
            ev.target.classList.remove('js-edit-button');
            let inEdit = document.querySelector(".in_edit");
            inEdit.classList.remove('in_edit');
            inEdit.querySelector('.el_check').removeAttribute('disabled');
            let taskSpan = inEdit.querySelector('.task_span');
            taskSpan.innerText = old_input.value;
            old_input.value = "";
            let elXGroup = document.querySelector('#clear-edit-group');
            elXGroup.classList.add('d-none');
            saveToStorage();
        }
    });

const elx = document.querySelector('#clear-edit');
elx.addEventListener('click', function (ev) {
    const addButtonEl = document.querySelector('#btn_new_task');
    addButtonEl.innerText = 'Добавить';
    addButtonEl.classList.add('js-add-button');
    addButtonEl.classList.remove('js-edit-button');
    let elXGroup = document.querySelector('#clear-edit-group');
    elXGroup.classList.add('d-none');
    let old_input = document.querySelector('#new_task');
    old_input.value = "";
    let inEdit = document.querySelector(".in_edit");
    inEdit.classList.remove('in_edit');
    inEdit.querySelector('.el_check').removeAttribute('disabled');
})

function saveToStorage() {
    let mlist = document.querySelector('.el_mlist');
    let list = mlist.innerHTML;
    localStorage.setItem('list', list);

}

function loadFromStorage() {
    let item = localStorage.getItem('list');
    if (item) {
        let mlist = document.querySelector('.el_mlist');
        mlist.innerHTML = item;
    }
}

loadFromStorage();
